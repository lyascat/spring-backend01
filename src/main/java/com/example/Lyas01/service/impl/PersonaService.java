package com.example.Lyas01.service.impl;

import com.example.Lyas01.entity.Persona;
import com.example.Lyas01.repository.PersonaRepository;
import com.example.Lyas01.service.IPersonaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.stream.Collectors;

public class PersonaService implements IPersonaService {

    // para llamar a la clase de repository tengo que llamar atraves
    // de la inyección de dependencias mediante Autowired
    @Autowired
    private PersonaRepository personaDao;

    @Override
    public List<Persona> listALL() {
        return this.personaDao.findAll();
    }

    @Override
    public List<Persona> listPersonasMayoresCinco() {
        return this.personaDao.findAll().stream().filter(p -> p.getEdad()> 5).collect(Collectors.toList());
    }

    @Override
    public Persona obtenerPersona(int id) {
        return null;
    }

    @Override
    public void deletePersona(int id) {

    }

    @Override
    public void saveOrUpdatePersona(Persona persona) {

    }

    @Override
    public Page<Persona> listAll(Pageable pageable) {
        return null;
    }
}
