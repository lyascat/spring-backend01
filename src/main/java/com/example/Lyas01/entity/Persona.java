package com.example.Lyas01.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
// se pone este nombre para que se genera en la base de datos
// de lo contrario en la base de datos se pondría Persona
@Table(name = "tb_persona")
@Data
public class Persona implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    // estos decoradores son pustos para generar un id automatico y que sean PK
    private int id;

    @Column(name = "nombres", nullable = false, length = 50)
    private  String nombres;

    @Column(name = "apeliido_paterno", nullable = false, length = 80)
    private  String apellidoPaterno;

    @Column(name = "apeliido_materno", nullable = false, length = 80)
    private  String apellidoMaterno;

    @Column(name= "dni", nullable = false, length = 8)
    private  String dni;

    @Transient
    private Integer edad;
}
