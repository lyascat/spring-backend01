package com.example.Lyas01.repository;

import com.example.Lyas01.entity.Persona;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonaRepository extends JpaRepository<Persona, Integer> {

//    @Query(value = "select p from Persona p where p.dni = :dni")
//    Persona obtenerPersona(@Param("dni") String dni);
//
//    Persona findByDni(String dni);
}
