package com.example.Lyas01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Lyas01Application {

	public static void main(String[] args) {
		SpringApplication.run(Lyas01Application.class, args);
	}

}
