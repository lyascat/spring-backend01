package com.example.Lyas01.service;

import com.example.Lyas01.entity.Persona;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface IPersonaService {

    List<Persona> listALL();

    List<Persona> listPersonasMayoresCinco();

    Persona obtenerPersona(int id);

    void deletePersona(int id);

    void saveOrUpdatePersona(Persona persona);

    Page<Persona> listAll(Pageable pageable);
}
